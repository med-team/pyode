Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/filipeabperes/Py3ODE
Upstream-Name: Py3ODE
Upstream-Contact: https://github.com/filipeabperes/Py3ODE/issues

Files: *
Copyright: 2004-2007 PyODE developers
                     Timothy Stranex <timothy@stranex.com>
                     Matthias Baas <baas@ira.uka.de>
                     Brett Hartshorn
                     Bernie Roehl
License: LGPL-2+ or BSD-3-clause

Files: debian/*
Copyright: 2006-2009 Yaroslav Halchenko <debian@onerussian.com>
License: LGPL-2+

License: LGPL-2+
    This package is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 .
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the LGPL license can be found in
 /usr/share/common-licenses/LGPL-2 file.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
 .
  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
 .
  Neither the names of ODE's copyright owner nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
